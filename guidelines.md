<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Application Guidelines](#application-guidelines)
  - [Timeline](#timeline)
  - [GitLab submission guidelines](#gitlab-submission-guidelines)
  - [Useful links](#useful-links)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Application Guidelines

Students should complete the following steps on or before 25 March (1900 UTC)
to get qualified (valid submission):

1. Own a GitLab.com account
1. Application is available in the `applications/` directory according to the
  [submission guidelines](#gitlab-submission-guidelines)
1. Application is submitted to Google

Missing one of the above will result in rejecting your application. Therefore
please be extra careful.

## Timeline

Make sure to familiarize yourself with the [GSoC 2016 timeline]
(https://developers.google.com/open-source/gsoc/timeline).

The notable dates and deadlines are depicted in the following table.

| Date | Event |
| ---- | ----- |
| 29 February - 13 March | Would-be student participants discuss application ideas with mentoring organizations. |
| 14 March 19:00 UTC | Student application period opens. |
| 25 March 19:00 UTC | Student application deadline. |

## GitLab submission guidelines

1. [Create a GitLab.com account][acc] if you don't have one already.
1. Login to GitLab.com and fork this repository.
1. Clone your repository:

    ```
    git clone git@gitlab.com:<username>/gsoc2016
    ```
1. Set the upstream Git remote:

    ```
    git remote set ustream https://gitlab.com/gitlab-org/gsoc2016.git
    ```

1. Create a new branch to work on:

    ```
    git checkout -b username_application
    ```

    where `username` is your GitLab.com username.

1. Copy the [application template](gitlab-application-template.md) to the
   `applications/` directory renaming it using the following scheme:

    ```
    cp gitlab-application-template.md applications/<Name>_<Surname>_application.md
    ```

    where `<Name>` is your name and `<Surname>` is your surname.

1. Edit your newly copied application and fill in the requested items by replacing
   the curly braces (`{}`) with your response.

1. Add and commit your changes:

    ```
    git add applications/<Name>_<Surname>_application.md
    git commit
    ```

1. Push the branch to your fork:

    ```
    git push origin username_application
    ```

1. Submit a merge request by visiting https://gitlab.com/gitlab-org/gitlab-ce/merge_requests

1. You can do any changes you want until the deadline.

## Useful links

- [Communication](communication.md) - Get in touch with the GitLab community
- [Ideas Page](ideas.md) - The recommended ideas for GSoC 2016 and the
  associated mentors
- [GitLab Application template](gitlab-application-template.md) - A template
  to use for your application
- [Development workflow](workflow.md) - The workflow of GitLab's development

[acc]: https://gitlab.com/users/sign_up
