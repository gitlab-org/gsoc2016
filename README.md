<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab GSoC 2016](#gitlab-gsoc-2016)
  - [Learn about the program](#learn-about-the-program)
  - [Find a project](#find-a-project)
  - [Start communicating](#start-communicating)
  - [Set up an account](#set-up-an-account)
  - [Create your Application](#create-your-application)
  - [Let others know](#let-others-know)
  - [Keep communicating](#keep-communicating)
  - [Useful links](#useful-links)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# GitLab GSoC 2016

Welcome to GitLab Google Summer of Code 2016!

In this page you will find all useful information regarding your application.

Note that there is a strict deadline on application submissions by Google, so
try to plan ahead.

## Learn about the program

The links below provide useful information about the Google Summer of Code
program:

- [GSoC student manual](https://developers.google.com/open-source/gsoc/resources/manual)
- [GSoC 2016 timeline](https://developers.google.com/open-source/gsoc/timeline)
- [Previous GSoC (2015)](https://www.google-melange.com/gsoc/homepage/google/gsoc2015)
- [GSoC official mailing lists](https://developers.google.com/open-source/gsoc/resources/lists)

## Find a project

Visit the [ideas page](ideas.md) to see the recommended GitLab ideas for GSoC
2016.

## Start communicating

Express your ideas and get in touch with the rest of the [GitLab community]
(communication.md).

## Set up an account

If you do not already have an account on GitLab.com,
[create one](https://gitlab.com/users/sign_in).

## Create your Application

Read carefully the [application guidelines](guidelines.md) to get you started
with your application.

We provide an [application template](gitlab-application-template.md) with
questions to get to know you better.

## Let others know

Let others know about your submission by forwarding your application URL with a
brief description about your proposal to the [GitLab mailing list]
(https://groups.google.com/forum/#!forum/gitlabhq).

## Keep communicating

We cannot overstress the importance of communication. Keep talking, and
listening, to the discussion group, to the sub-projects relevant to your
proposal and to potential mentors. Be patient, as mentors and other contributors
are often very busy people.

## Useful links

- [Communication](communication.md) - Get in touch with the GitLab community
- [Ideas Page](ideas.md) - The recommended ideas for GSoC 2016 and the
  associated mentors
- [GitLab application guidelines](guidelines.md) - Learn what are the
  requirements for a successful application
- [GitLab Application template](gitlab-application-template.md) - A template
  to use for your application
- [Development workflow](workflow.md) - The workflow of GitLab's development
