## Development workflow

All development is done in GitLab's [public issue tracker][issues].

Read more and get familiar with the [GitLab workflow](http://doc.gitlab.com/ce/workflow/README.html).

[issues]: https://gitlab.com/gitlab-org/gitlab-ce/issues
