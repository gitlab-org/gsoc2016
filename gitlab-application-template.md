Applications must be made in text format - you may use [GitLab flavored Markdown](/help/markdown/markdown.md).

## Contact information

* Your Name: {required}
* Email Address: {required}
* GitLab Username: {required}
* GitHub Username: {required}
* Twitter Username: {optional}
* IM Address: {optional}
* IRC Nickname: {optional}
* Webpage/Blog: {optional}
* College/University: {required}
* Subject/Major: {required}

## Project

### Project Name

{Your project's name}

### Project Description

{Your project's description}

### Why did you choose this idea?

{Your response}

### Please describe an outline project architecture or an approach to it

{Your response}

### Give us details about the milestones for this project

{Your response}

### Why will your proposal benefit GitLab?

{Your response}

## Open Source

### Please describe any previous Open Source development experience if any

{Your response}

### Why are you interested in Open Source?

{Your response}

## Availability

### How long will the project take? When can you begin?

{Your response}

### How much time do you expect to dedicate to this project? (weekly)

{Your response}

### Where will you be based during the summer?

{Your response}

### What timezone will you be working in and what hours do you plan to work? (so we can best match mentors)

{Your response}

### Do you have any commitments for the summer? (holidays/work/summer courses)

{Your response}

## Other

### Have you ever participated in a previous GSoC? If yes, describe your project.

{Your response}

### Have you applied for any other 2016 Summer of Code projects? If yes, which ones?

{Your response}

### Why did you apply for the Google Summer of Code ?

{Your response}

### Why did you choose GitLab as a mentoring organization?

{Your response}

### Why do you want to participate and why should GitLab choose you?

{Your response}
