You can communicate with the rest of the GitLab community by one of the
following ways:

- Open a new topic in [our forum](https://forum.gitlab.com/c/gsoc/gsoc-2016)
- Subscribe to [our mailing list](https://groups.google.com/forum/#!forum/gitlabhq)
  and start a new discussion
- Ask a question in [our IRC channel](https://webchat.freenode.net/?channels=gitlab)
