<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Proposal ideas](#proposal-ideas)
  - [Internationalization - translations](#internationalization-translations)
  - [Mailing list functionality](#mailing-list-functionality)
  - [Transactional merge request comments](#transactional-merge-request-comments)
  - [Cross-server (federated) merge requests](#cross-server-federated-merge-requests)
- [Mentors](#mentors)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Proposal ideas

This page hosts the ideas GitLab proposes for Google Summer of Code 2016.
For more information about the program, make sure to read the [home page](README.md).

You can [discuss](communication.md) the ideas posted here or create a new topic
with your idea in our [forum](https://forum.gitlab.com/c/gsoc/gsoc-2016) (tip:
you can login using your GitLab.com credentials).

---

Below are some ideas to get you started. The list is not exhaustive, you are
encouraged to take a look at **[GitLab's direction page](https://about.gitlab.com/direction/)**
for more items.

## Redesign and improve functionality of GitLab's Status Page

- **Relevant Issue:** https://gitlab.com/gitlab-org/status-gitlab-com/issues/2
- **Skills:** Rails, HTML, CSS

Our [Status Page](status-page) for GitLab.com is currently pretty barebones.
It'd be nice if we had a more fully-featured Status page, a la [GitHub](github-status), [Slack](slack-status), [Stripe](stripe-status), or [Heroku](heroku-status). It should allow us to easily, but securely update the status page with GitLab.com's system status, provide some simple API endpoints, include recent history, and ideally be reusable by other projects and companies.

[status-page]: https://status.gitlab.com
[github-status]: https://status.github.com
[slack-status]: https://status.slack.com
[stripe-status]: https://status.stripe.com
[heroku-status]: https://status.heroku.com


## Internationalization - translations

- **Relevant Issue:** https://gitlab.com/gitlab-org/gitlab-ce/issues/4012
- **Skills:** Rails

It would be nice if GitLab's UI could be translated to other languages than
English. This feature must be implemented in a way that the community could
easily provide their translations. You don't need to be able to speak another
language for this project! This would just be setting up the GitLab Rails app
to use localizable elements. There is an ongoing discussion in the issue
linked above where you can find more information.

## Mailing list functionality

- **Relevant Issue:** https://gitlab.com/gitlab-org/gitlab-ce/issues/4272
- **Skills:** Rails, some theory on how the various mail protocols work

Right now, everyone that wants a mailing list for their project is required to
use an external one like Google Groups.

It would be nice if this could be done with GitLab. We already have the ability
to read email for the [reply-to-email feature][inc-email]. Below are some rough
points that would need to be implemented for this feature:

1. Give each project a mailing list at `project/group@example.com`
2. People joining the project are subscribed to this mailing list by default
3. Only people with an account on the GitLab server can post
4. Every email has a link that unsubscribes them without logging in

[inc-email]: http://doc.gitlab.com/ce/incoming_email/README.html

## Transactional merge request comments

- **Relevant Issue:** https://gitlab.com/gitlab-org/gitlab-ce/issues/3364
- **Skills:** Rails

The problem is that for each review or inline-code comment, a notification is
sent out for each individual comment, which creates a lot of noise when
receiving email notifications.

One approach is to allow the review to create/draft many comments together and
submit them all to be notified in batch (like Phabricator does it). The result
is that only one notification/email gets sent out with all the comments and
feedback contained within it. This mimics a review pass by the reviewer in that
all feedback is captured by that one pass so that the individual can see the
entire (holistic) feedback/comments instead of having them be itemized
individually with no clear connection.

## Cross-server (federated) merge requests

- **Relevant Issue:** https://gitlab.com/gitlab-org/gitlab-ce/issues/4013
- **Skills:** Rails

Federation is very important; one of the biggest problems with the modern
Internet is that the web has become totally centralized.

This proposal fits with Git's distributed nature, since it would allow
collaboration across different GitLab servers or even to other services such as
GitHub. You could e.g. fork a repository hosted on a company GitLab server to
GitLab.com and still do merge requests.

# Mentors

- [Drew Blessing](https://gitlab.com/u/dblessing)
- [Jeroen van Baarsen](https://gitlab.com/u/jvanbaarsen)
- [Grzegorz Bizon](https://gitlab.com/u/grzesiek)
- [Connor Shea](https://gitlab.com/u/connorshea)
